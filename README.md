# Composer package for querying GLS parcelshops in CEE countries

This package can retrieve info about GLS (General Logistics Systems) parcelshops located in the CEE (Central and Eastern European) region, specifically:
- CZ (Czech Republic)
- HR (Croatia)
- HU (Hungary)
- RO (Romania)
- SI (Slovenia)
- SK (Slovakia)

While tehere is a [SOAP web service on GLS.dk][1] supporting multiple countries
to query parcelshops from, it returns an empty list for the above ones. The
[GLS Netherlands API][2] requires authentication and seems to publish only Dutch
parcelshops.

This leaves us with studying [a local parcelshop search's JavaScript][3], which
is aware of the region. The consumed web service is undocumented, so You may
**expect it to disappear or mutate any moment**. Until there's an official API
for the region, this is your best shot. This package tries to detect unexpected
behavior and throws an exception in such cases.

## Installation
```
composer require kerekit/gls-ee-parcelshops
```

## Usage
```php
use Kerekit\GlsParcelshopsCee\{Parcelshop,CountryCode};

/**
 * List parcelshops in Hungary.
 *
 * NOTE: "ctrcode" is required, but you can use more filters (see
 * \Kerekit\GlsParcelshopsCee\Request\GetList properties), but results seem to
 * be inconsequent.
 */
$parcelshops = Parcelshop::list (['ctrcode' => CountryCode::HU ()]);

// Display some info about the first parcelshop.
$p = $parcelshops [0];
echo "The first shop's name is '$p->name'. You can call them on '$p->phone'.\n";

/**
 * Retrieve & display some opening hours for the first parcelshop.
 *
 * NOTE: Opening indexes range from 1 to 7 - Monday to Sunday!
 */
$openings = $p->getOpenings ();
$mon = $openings [1] ?? null;
if (!is_null ($mon)) {
    echo "Opening hours on Monday: $mon->open (lunch break: $mon->midbreak).\n";
}
$sun = $openings [7] ?? null;
if (!is_null ($sun)) {
    echo "Opening hours on Sunday: $sun->open (lunch break: $sun->midbreak).\n";
}
```

[1]: https://www.gls.dk/webservices_v4/wsShopFinder.asmx (GLS.dk web service)  
[2]: https://api-portal.gls.nl/ (GLS Netherlands API)  
[3]: https://online.gls-hungary.com/psmap/psmap.js (Hungarian search script)  
