<?php

namespace Kerekit\GlsParcelshopsCee;

class LanguageCode extends \MyCLabs\Enum\Enum
{
    public const CZ = 'cz';
    public const EN = 'en';
    public const HR = 'hr';
    public const HU = 'hu';
    public const RO = 'ro';
    public const SI = 'si';
    public const SK = 'sk';
}
