<?php

namespace Kerekit\GlsParcelshopsCee;

class Parcelshop extends AbstractBase
{
    public string      $address;
    public string      $city;
    public ?string     $contact;
    public CountryCode $ctrcode;
    public bool        $dropoffpoint;
    public ?string     $email;
    public float       $geolat;
    public float       $geolng;
    public ?\DateTime  $holidayends;
    public ?\DateTime  $holidaystarts;
    public ?string     $info;
    public bool        $iscodhandler;
    public bool        $isparcellocker;
    public string      $name;
    public string      $owner;
    public bool        $paybybankcard;
    public ?string     $pcl_pickup_time;
    public string      $pclshopid;
    public ?string     $phone;
    public ?string     $vendor_url;
    public string      $zipcode;

    /**
     * @see Request\GetList::__construct() for params.
     * @see Request\GetList::send()        for return type.
     */
    public static function list (...$args)
    {
        $request = new Request\GetList (...$args);
        return $request->send ();
    }

    public function getOpenings (): Openings
    {
        $request = new Request\GetOpenings ([
            'ctrcode'   => $this->ctrcode,
            'pclshopid' => $this->pclshopid,
        ]);
        return $request->send ();
    }
}
