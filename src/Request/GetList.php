<?php

namespace Kerekit\GlsParcelshopsCee\Request;

use Kerekit\GlsParcelshopsCee\{CountryCode,Parcelshop};

/** Query a filtered/unfiltered list of parcelshops in given country. */
class GetList extends AbstractRequest
{
    /** @var bool|null Probably filters for Parcelshop::$iscodhandler */
    public ?bool $codhandler = null;

    /** @var bool|null Probably filters for Parcelshop::$dropoffpoint */
    public ?bool $dropoff = null;

    /** @var bool|null Probably filters for Parcelshop::$isparcellocker */
    public ?bool $parcellockin = null;

    /** @todo Document purpose. */
    public ?bool $pclshopin = null;

    /** @todo Document purpose. */
    public ?string $senderid = null;

    /** @return Parcelshop[] */
    public function send (): array
    {
        $parcelshopsData = parent::send ();
        $parcelshops = [];
        foreach ($parcelshopsData as $d) {
            $parcelshops [] = new Parcelshop ([
                'address'         => $d ['address'],
                'city'            => $d ['city'],
                'contact'         => $d ['contact'],
                'ctrcode'         => new CountryCode ($d ['ctrcode']),
                'dropoffpoint'    => self::_parseBool ($d ['dropoffpoint']),
                'email'           => $d ['email'],
                'geolat'          => $d ['geolat'],
                'geolng'          => $d ['geolng'],
                'holidayends'     => self::_parseDate ($d ['holidayends']),
                'holidaystarts'   => self::_parseDate ($d ['holidaystarts']),
                'info'            => $d ['info'],
                'iscodhandler'    => self::_parseBool ($d ['iscodhandler']),
                'isparcellocker'  => self::_parseBool ($d ['isparcellocker']),
                'name'            => $d ['name'],
                'owner'           => $d ['owner'],
                'paybybankcard'   => self::_parseBool ($d ['paybybankcard']),
                'pcl_pickup_time' => $d ['pcl_pickup_time'],
                'pclshopid'       => $d ['pclshopid'],
                'phone'           => $d ['phone'],
                'vendor_url'      => $d ['vendor_url'],
                'zipcode'         => $d ['zipcode'],
            ]);
        }
        return $parcelshops;
    }
}
