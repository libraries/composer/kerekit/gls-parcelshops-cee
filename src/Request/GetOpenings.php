<?php

namespace Kerekit\GlsParcelshopsCee\Request;

use Kerekit\GlsParcelshopsCee\{Opening,Openings};

/** Queries opening hours for the requested parcelshop ID. */
class GetOpenings extends AbstractRequest
{
    /** @var string Parcelshop ID to request opening hours for. */
    public string $pclshopid;

    public function send (): Openings
    {
        /** Some parcelshop openings are just unavailable these days. It may be
         *  a temporary service error, it's best to ignore and wait...
         */
        try {
            $openingsData = parent::send ();
        } catch (\Exception $e) {
            $openingsData = [];
        }

        $openingItems = array_fill (1, 7, new Opening ([
            'midbreak' => '',
            'open'     => '',
        ]));
        foreach ($openingsData as $openingData) {
            switch ($openingData ['day']) {
                case 'monday':    $dayOfWeek = 1; break;
                case 'tuesday':   $dayOfWeek = 2; break;
                case 'wednesday': $dayOfWeek = 3; break;
                case 'thursday':  $dayOfWeek = 4; break;
                case 'friday':    $dayOfWeek = 5; break;
                case 'saturday':  $dayOfWeek = 6; break;
                case 'sunday':    $dayOfWeek = 7; break;
                default:
                    throw new \Exception ("Unexpected opening day: '$openingData[day]'.");
                    break;
            }
            $openingItems [$dayOfWeek] = new Opening ([
                'midbreak' => $openingData ['midbreak'],
                'open'     => $openingData ['open'],
            ]);
        }
        return new Openings ($openingItems);
    }
}
