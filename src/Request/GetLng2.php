<?php

namespace Kerekit\GlsParcelshopsCee\Request;

/**
 * Queries frontend messages for requested country and language. You probably
 * don't need it in your app, it's just here for the sake of completeness.
 */
class GetLng2 extends AbstractRequest
{
    /**
     * @var LanguageCode $country Language (not country!) code to retrieve
     *                            frontend messages for. The property is named
     *                            confusingly according to the original param
     *                            name.
     */
    public LanguageCode $country;
}
