<?php

namespace Kerekit\GlsParcelshopsCee\Request;

use Kerekit\GlsParcelshopsCee\{AbstractBase,CountryCode};

/**
 * Public properties represent the available (required & optional) request
 * parameters & filters.
 */
abstract class AbstractRequest extends AbstractBase
{
    protected const ENDPOINT_DOMAINS = [
        'CZ' => 'gls-czech.com',
        'HR' => 'gls-croatia.com',
        'HU' => 'gls-hungary.com',
        'RO' => 'gls-romania.ro',
        'SI' => 'gls-slovenia.com',
        'SK' => 'gls-slovakia.sk',
    ];

    protected const ENDPOINT_FORMAT = 'https://online.%s/psmap/psmap_getdata.php';

    /** @var CountryCode Required param for all requests, defines endpoint. */
    public CountryCode $ctrcode;

    /** @return string The name of the action used in the query */
    protected static function _getAction (): string
    {
        $reflection = new \ReflectionClass (static::class);
        $shortName = $reflection->getShortName ();
        return lcfirst ($shortName);
    }

    protected static function _parseBool (string $boolStr): bool
    {
        switch ($boolStr) {
            case 'f': return false;
            case 't': return true;
            default: throw new \Exception ("Unexpected bool: '$boolStr'.");
        }
    }

    protected static function _parseDate (string $dateStr = null): ?\DateTime
    {
        if (is_null ($dateStr)) {
            return null;
        }
        $date = \DateTime::createFromFormat (
            'Y-m-d H:i:s.u',
            "$dateStr 00:00:00.000000"
        );
        if ($date === false) {
            throw new \Exception ("Failed to parse date string: '$dateStr'.");
        }
        return $date;
    }

    /**
     * @return mixed Response parsed as JSON data. May be specified in child.
     *
     * @throws \Exception If request fails or response can't be parsed as JSON.
     */
    public function send ()
    {
        // Init request
        $endpoint = $this->_getEndpoint ();
        $reflection = new \ReflectionClass ($this);
        $params = ['action' => $this->_getAction ()];
        $properties = $reflection->getProperties ();
        foreach ($properties as $property) {
            $value = $property->getValue ($this);
            if (is_null ($value)) {
                continue;
            }
            $params [$property->name] = "$value";
        }
        $query = http_build_query ($params);

        // Make request
        $body = file_get_contents ("$endpoint?$query");
        if ($body === false) {
            throw new \Exception ("Failed to make request.");
        }

        // Parse response
        $response = json_decode ($body, true);
        $errno = json_last_error ();
        if ($errno !== JSON_ERROR_NONE) {
            $error = json_last_error_msg ();
            throw new \Exception ("Failed to parse response as JSON. Error message: $error");
        }

        // Failed request (probably a bad request)
        if ($response === false) {
            throw new \Exception ("Response returned false (it's probably a bad request).");
        }

        return $response;
    }

    protected function _getEndpoint (): string
    {
        $domain = self::ENDPOINT_DOMAINS ["$this->ctrcode"] ?? null;
        if (is_null ($domain)) {
            throw new \Exception ("No domain defined for ctrcode '$this->ctrcode'.");
        }
        return sprintf (self::ENDPOINT_FORMAT, $domain);
    }
}
