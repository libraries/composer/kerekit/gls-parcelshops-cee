<?php

namespace Kerekit\GlsParcelshopsCee;

class CountryCode extends \MyCLabs\Enum\Enum
{
    public const CZ = 'CZ';
    public const HR = 'HR';
    public const HU = 'HU';
    public const RO = 'RO';
    public const SI = 'SI';
    public const SK = 'SK';
}
