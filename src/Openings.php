<?php

namespace Kerekit\GlsParcelshopsCee;

class Openings implements \ArrayAccess, \Iterator
{
    /** @var int Current day of week in ISO-8601 format (1-7 as Mon-Sun) */
    protected int $position = 1;

    /** @var Opening[] Array indexes are the ISO-8601 day of the week */
    protected array $openings;

    /** @param Opening[] $openings */
    public function __construct (array $openings)
    {
        // Check count
        $count = count ($openings);
        if ($count !== 7) {
            throw new \Exception ("Openings::__construct() expects exactly 7 array items, received $count instead.");
        }

        // Check indexes and types
        foreach ($openings as $dayOfWeek => $opening) {
            if (!($opening instanceof Opening)) {
                throw new \Exception ("Openings::__construct() expects an array of Opening instances.");
            }
            if (!is_int ($dayOfWeek) || $dayOfWeek < 1 || $dayOfWeek > 7) {
                throw new \Exception ("Openings::__construct() expects array indexes to be the day of the week in ISO-8601 format (1-7 as Monday-Sunday).");
            }
        }

        // Re-order by day of week
        ksort ($openings, SORT_NUMERIC);

        $this->openings = $openings;
    }

    public function current (): Opening
    {
        return $this->openings [$this->position];
    }

    public function key (): int
    {
        return $this->position;
    }

    public function next (): void
    {
        ++$this->position;
    }

    public function offsetExists ($offset): bool
    {
        return array_key_exists ($offset, $this->openings);
    }

    public function offsetGet ($offset): Opening
    {
        return $this->openings [$offset];
    }

    public function offsetSet ($offset, $value): void
    {
        throw new \Exception ("Openings data is immutable.");
    }

    public function offsetUnset ($offset): void
    {
        throw new \Exception ("Openings data is immutable.");
    }

    public function rewind (): void
    {
        $this->position = 1;
    }

    public function valid (): bool
    {
        return array_key_exists ($this->position, $this->openings);
    }
}
