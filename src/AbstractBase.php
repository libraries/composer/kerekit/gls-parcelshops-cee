<?php

namespace Kerekit\GlsParcelshopsCee;

/** Sets properties from an associated array provided to the constructor. */
abstract class AbstractBase
{
    /**
     * @param array $data (optional) Associative array for setting properties
     *
     * @throws \Exception if setting undefined properties.
     * @throws \Exception if a required property (without default) is missing.
     */
    public function __construct (array $data = [])
    {
        // Collect all & required property names and data keys
        $reflection = new \ReflectionClass ($this);
        $allProperties = $reflection->getProperties ();
        $requiredProperties = array_filter (
            $allProperties,
            fn($p) => !$p->isInitialized ($this)
        );
        $allPropertyNames = array_map (
            fn ($p) => $p->name,
            $allProperties
        );
        $requiredPropertyNames = array_map (
            fn ($p) => $p->name,
            $requiredProperties
        );
        $dataKeys = array_keys ($data);

        // Check if required properties are missing from data
        $missingPropertyNames = array_diff ($requiredPropertyNames, $dataKeys);
        if (count ($missingPropertyNames)) {
            $msg = sprintf (
                'Requied properties missing: %s',
                implode (', ', $missingPropertyNames)
            );
            throw new \Exception ($msg);
        }

        // Check if data contains undefined properties
        $undefinedPropertyNames = array_diff ($dataKeys, $allPropertyNames);
        if (count ($undefinedPropertyNames)) {
            $msg = sprintf (
                'Trying to set undefined properties: %s',
                implode (', ', $undefinedPropertyNames)
            );
        }

        // Set properties from array
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }
}
