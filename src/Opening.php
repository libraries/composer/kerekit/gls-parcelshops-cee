<?php

namespace Kerekit\GlsParcelshopsCee;

/**
 * It would be nice to use a special time period type for $midbreak & $open
 * properties, but unsanitized entries make it impossible. Format varies, even
 * mixed ones occur such as "hh:mm:hh:mm". Expect empty strings and different
 * words meaning "closed" or "pause" in local language as well.
 */
class Opening extends AbstractBase
{
    public string $midbreak;
    public string $open;
}
