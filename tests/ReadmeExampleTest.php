<?php declare(strict_types=1);

namespace Kerekit\GlsParcelshopsCee;

use PHPUnit\Framework\TestCase;
use Kerekit\GlsParcelshopsCee\{Openings,Opening,Parcelshop,CountryCode};

final class ReadmeExampleTest extends TestCase
{
    public function testCanRunReadmeExampleAsExpected (): void
    {
        $parcelshops = Parcelshop::list (['ctrcode' => CountryCode::HU ()]);
        $p = $parcelshops [0];
        $this->assertInstanceOf (Parcelshop::class, $p);
        $openings = $p->getOpenings ();
        $this->assertInstanceOf (Openings::class, $openings);
        $opening = $openings [1] ?? null;
        $this->assertInstanceOf (Opening::class, $opening);
        $opening = $openings [7] ?? null;
        $this->assertInstanceOf (Opening::class, $opening);
    }
}
